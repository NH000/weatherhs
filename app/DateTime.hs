{-
 - Copyright (C) 2022 Nikola Hadžić
 -
 - This file is part of weatherhs.
 -
 - weatherhs is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 - 
 - weatherhs is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 - 
 - You should have received a copy of the GNU General Public License
 - along with weatherhs.  If not, see <https://www.gnu.org/licenses/>.
 -}

module DateTime where

import  Data.Time.Clock         (getCurrentTime, utctDay)
import  Data.Time.Calendar      (toGregorian)

-- Get current date as a triple of year, month and day.
getCurrentDate :: IO (Integer, Int, Int)
getCurrentDate = getCurrentTime >>= return . toGregorian . utctDay
