# weatherhs

## Description
Command-line tool that retrieves and nicely outputs weather and related data from [Weather API](https://www.weatherapi.com/).

Written in Haskell.

## Dependencies

#### Building & installing
+ stack
+ msgfmt
+ make

#### Running
+ gettext
+ gpgme
+ pcre

## Installation
Use Stack to build and Make to install this project:
Enter the project's root directory and run:

```
stack build weatherhs --ghc-options=-U__MESSAGE_CATALOG_DIR__
make install
```

You'll need to change `__MESSAGE_CATALOG_DIR__` value to point to your locale directory; the default (when undefined externally) is "/usr/share/locale".

You can pass `--ghc-options=-dynamic` to Stack to link with dynamic libraries; note that then you'll need to have those libraries installed to run the program.

Run `make help` for more installation options.

## Uninstallation
Enter the project's root directory and run:

```
make uninstall
```

Make sure that Make variables are set to the values used for the installation.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions** |
|--------------------|----------------------------------------------------------|------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0.0            |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0.0            |

### Translation process
This program is written to be easily translatable into multiple languages, and it achieves that through the use of [`gettext`](https://www.gnu.org/software/gettext/) library.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

To add a new or update existing translation, enter the project's `po` directory and run the following command:

```
make %.po  # Generate/update PO file; "%" should be replaced with a language code.
```

Afterwards, you can edit created/updated PO file (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details),
translating the program that way.

You could also run `make messages.pot` to just generate the template file, but this will be done automatically by the previously described rule.

**NOTE:** You need to have hgettext tool, msgcat and sed installed in order to generate the template file.

**NOTE:** Make sure to use hgettext tool with this commit: <https://github.com/NH002/hgettext/commit/a7ba5f7bc2624b91ddbbeeff7846947f56605c45>.

It would also be good to translate the manual page, you will find it in "man" project subdirectory.
In that case also update .cabal file to include new manual page translation.
