# Installation commands.
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# Target directories.
PREFIX := /usr
BINDIR := $(PREFIX)/bin
DATADIR := $(PREFIX)/share
LOCALEDIR := $(DATADIR)/locale
MANDIR := $(DATADIR)/man

# Deletes file $(2) from directory $(1), then deletes empty parent directories subsequently.
delete_file_and_empty_dirs = if test -d '$(1)'; then rm -f '$(1)/$(2)' && rmdir -p --ignore-fail-on-non-empty '$(1)'; fi

# Recursively installs manual pages to subdirectory $(1) of $(MANDIR).
install_man = $(foreach f,$(notdir $(wildcard man/$(1)/*)),$(if $(shell test -d 'man/$(1)/$(f)' && echo "dir"),$(call install_man,$(1)/$(f)),$(INSTALL_DATA) -D -t '$(MANDIR)/$(1)' 'man/$(1)/$(f)';))

# Recursively removes installed manual pages from subdirectory $(1) of $(MANDIR).
uninstall_man = $(foreach f,$(notdir $(wildcard man/$(1)/*)),$(if $(shell test -d 'man/$(1)/$(f)' && echo "dir"),$(call uninstall_man,$(1)/$(f)),$(call delete_file_and_empty_dirs,$(MANDIR)/$(1),weatherhs.1);))

.PHONY: help install uninstall

help:
	$(info VARIABLES)
	$(info ========================================================================)
	$(info PREFIX:      Target directories prefix.)
	$(info BINDIR:      Executable installation directory.)
	$(info DATADIR:     Data installation directory.)
	$(info LOCALEDIR:   MO files installation directory.)
	$(info MANDIR:      Manual pages installation directory.)
	$(info )
	$(info RULES)
	$(info ========================================================================)
	$(info help:        Display this help menu.)
	$(info install:     Install the executable, its assets and its manual pages.)
	$(info uninstall:   Uninstall the executable, its assets and its manual pages.)

install: .stack-work/dist/x86_64-linux-tinfo6/Cabal-2.2.0.1/build/weatherhs/weatherhs po/*.po man/
	$(INSTALL) -D -t '$(BINDIR)' '$<'
	$(foreach lc,$(patsubst po/%.po,%,$(wildcard po/*.po)),mkdir -p '$(LOCALEDIR)/$(lc)/LC_MESSAGES' && msgfmt -o '$(LOCALEDIR)/$(lc)/LC_MESSAGES/weatherhs.mo' 'po/$(lc).po';)
	$(call install_man)

uninstall:
	rm -f '$(BINDIR)/weatherhs'
	$(foreach lc,$(patsubst po/%.po,%,$(wildcard po/*.po)),$(call delete_file_and_empty_dirs,$(LOCALEDIR)/$(lc)/LC_MESSAGES,weatherhs.mo);)
	$(call uninstall_man)
